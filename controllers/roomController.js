const { Room } = require("../models");

module.exports = {
  show: async (req, res) => {
    try {
      const rooms = await Room.findAll();
      res.status(200).json(rooms);
    } catch (error) {
      res.json({ error: error });
    }
  },
  create: async (req, res) => {
    try {
      const { name } = req.body;
      const room = await Room.create({ name });
      res.status(200).json(room);
    } catch (error) {
      res.json({ error: error });
    }
  },
};
