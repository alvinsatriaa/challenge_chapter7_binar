const { GameHistoryDetail, User } = require("../models");

function getRoundResult(gameDetail1, gameDetail2) {
  if (gameDetail1.choice === gameDetail2.choice) {
    return "DRAW";
  }
  const playersOption = gameDetail1.choice + gameDetail2.choice;
  // let gameResult = [];
  switch (playersOption) {
    case "RS":
    case "PR":
    case "SP":
      return (gameResult = [
        `${gameDetail1.User.username} Wins`,
        `${gameDetail2.User.username} Lose`,
      ]);

    case "SR":
    case "RP":
    case "PS":
      return (gameResult = [
        `${gameDetail2.User.username} Wins`,
        `${gameDetail1.User.username} Lose`,
      ]);
  }
}

function getOverallResult(gameDetailsPerRoom) {
  //logic here

  return "User with username player1 Win";
}

function getRPSResult(gameDetailsPerRoom) {
  const dataRounds = [];
  let round = 1;
  for (let i = 0; i < gameDetailsPerRoom.length; i += 2) {
    dataRounds.push({
      round,
      result: getRoundResult(gameDetailsPerRoom[i], gameDetailsPerRoom[i + 1]),
      choices: [
        {
          user_id: gameDetailsPerRoom[i].user_id,
          choice: gameDetailsPerRoom[i].choice,
        },
        {
          user_id: gameDetailsPerRoom[i + 1].user_id,
          choice: gameDetailsPerRoom[i + 1].choice,
        },
      ],
    });
    round++;
  }

  return {
    overallResult: getOverallResult(gameDetailsPerRoom, user_id),
    rounds: dataRounds,
  };
}

module.exports = {
  show: async (req, res, next) => {
    const gameDetail = await GameHistoryDetail.findAll({
      where: { room_id: +req.params.showId },
    });
    res.json(gameDetail);
  },
  fight: async (req, res, next) => {
    const roomId = +req.params.roomId;
    const { choice } = req.body;
    const id = +req.user.id;

    await GameHistoryDetail.create({
      room_id: roomId,
      user_id: id,
      choice: choice,
    });

    let gameDetailsPerRoom = await GameHistoryDetail.findAll({
      where: { room_id: roomId },
      include: User,
    });
    if (gameDetailsPerRoom.length % 2 === 1) {
      const intervalId = setInterval(async () => {
        gameDetailsPerRoom = await GameHistoryDetail.findAll({
          where: { room_id: roomId },
          include: User,
        });

        if (gameDetailsPerRoom.length % 2 === 0) {
          clearInterval(intervalId);

          res.json(getRPSResult(gameDetailsPerRoom));
        }
      }, 5000);
    } else {
      res.json(getRPSResult(gameDetailsPerRoom));
    }
  },
};
