var express = require("express");
var router = express.Router();
const user = require("../controllers/userController");
const isAuthenticated = require("../middlewares/isAuthenticated");

router.post("/register", user.register);
router.post("/login", user.login);
router.get("/whoami", user.whoami);
module.exports = router;
